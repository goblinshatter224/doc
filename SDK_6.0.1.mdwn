**SDK 6.0.1r31 (GNU/Linux, Windows)**

[[!toc]]

## Recreate matching build environment

The current reference build environment for this version is [[Ubuntu 14.04|Build_environments]].

Source code was pushed between 2015-10-05 ([manifest commit](https://android.googlesource.com/platform/manifest/+/refs/heads/android-6.0.0_r1)) and 2015-10-08 (starting this page).

## Build dependencies

    # http://source.android.com/source/initializing.html + fixes
    dpkg --add-architecture i386
    apt-get update
    apt-get -y install openjdk-7-jdk
    apt-get -y install bison git gperf libxml2-utils make python-networkx zip unzip
    apt-get -y install g++-multilib lib32ncurses5 zlib1g-dev:i386

    # Windows dependencies
    # https://sites.google.com/a/android.com/tools/build  2015-07-23
    apt-get -y install mingw32 tofrodos


## Prepare source tree

Install `repo`:

    sudo apt-get install curl ca-certificates git python
    mkdir ~/bin/
    curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
    chmod a+x ~/bin/repo

    # avoid prompts
    git config --global user.email "you@example.com"
    git config --global user.name "Your Name"
    git config --global color.ui true

Checkout the source:

    mkdir ~/wd/
    cd ~/wd/
    ~/bin/repo init -u https://android.googlesource.com/platform/manifest -b android-6.0.1_r31
    #~/bin/repo init -u https://android.googlesource.com/platform/manifest -b android-6.0.1_r81  # win_sdk fails
    time ~/bin/repo sync
    # 1h30 with fiber connection
    # 44GB (inc. 28GB .repo, from scratch)

## Build the SDK

    # Optional: reserve 10GB for ccache if you're investigating how to build or patching something
    # Cf. https://source.android.com/source/initializing.html#ccache
    export USE_CCACHE=1
    prebuilts/misc/linux-x86/ccache/ccache -M 10G

    . build/envsetup.sh
    lunch sdk-eng
    time make sdk -j$(nproc) showcommands
    # 80GB (inc. 25GB .repo), 7.5GB cache size, 8GB RAM direly needed during the 'docdroid' phase
    # => out/host/linux-x86/sdk/sdk/android-sdk_eng.android_linux-x86.zip
    # rebuilt in N/Amn using N/AGB ccache

    time make win_sdk -j$(nproc)
    # 95GB (inc. 28GB .repo), ccache untouched
    # => out/host/windows/sdk/sdk/android-sdk_eng.android_windows.zip


## TODO

Rebuild the `prebuilts/` sub-directories.


## Error messages and resolutions

This occurs when requesting a `sdk` and a `win_sdk` in `user` build variant successively. No real answer, besides switching back to `eng` build variant.

    $ lunch sdk-user
    $ make sdk -j$(nproc) showcommands
    $ make win_sdk -j$(nproc) showcommands
    ...
    dex2oatd E 20617 20617 art/runtime/gc/heap.cc:258] Could not create image space with image file 'out/target/product/generic/dex_bootjars/system/framework/boot.art'. Attempting to fall back to imageless running. Error was: No place to put generated image.
    dex2oatd E 20629 20629 art/runtime/gc/heap.cc:258] Could not create image space with image file 'out/target/product/generic/dex_bootjars/system/framework/boot.art'. Attempting to fall back to imageless running. Error was: No place to put generated image.
    dex2oatd E 20617 20617 art/runtime/runtime.cc:884] Dex file fallback disabled, cannot continue without image.
    dex2oatd E 20629 20629 art/runtime/runtime.cc:884] Dex file fallback disabled, cannot continue without image.
    dex2oatd E 20617 20617 art/dex2oat/dex2oat.cc:1694] Failed to create runtime
    dex2oatd E 20629 20629 art/dex2oat/dex2oat.cc:1694] Failed to create runtime
    dex2oatd I 20629 20629 art/dex2oat/dex2oat.cc:1846] dex2oat took 31.493ms (threads: 4) 
    dex2oatd I 20617 20617 art/dex2oat/dex2oat.cc:1846] dex2oat took 31.926ms (threads: 4) 
    make: *** [out/target/product/generic/obj/APPS/CalendarProvider_intermediates/oat/arm/package.odex] Error 1

This occurs when compiling `win_sdk` the newest r81. r31 works OK.

    PRODUCT_COPY_FILES frameworks/base/data/sounds/effects/ogg/VideoRecord_48k.ogg:system/media/audio/ui/VideoRecord.ogg ignored.
    PRODUCT_COPY_FILES frameworks/base/data/sounds/effects/ogg/camera_click_48k.ogg:system/media/audio/ui/camera_click.ogg ignored.
    make[1]: Entering directory `/home/android/wd'
    make[1]: *** No rule to make target `adb', needed by `PRODUCT-sdk-aapt'.  Stop.
    make[1]: Leaving directory `/home/android/wd'
    make: *** [winsdk-tools] Error 2
    make: *** [Makefile:7: all] Error 2

## Notes

### Build result recap

* Tools/
  * [missing] Android [[SDK Tools]]
  * Android SDK Platform-tools 23 (update available r23.1??)
  * Android SDK Build-tools 23.0.2
* Android 6.0.1 (API 23)/
  * Documentation for Android SDK 23r1
  * SDK Platform 23r1 (update available r23r2??)
  * Samples for SDK 23r3
  * ARM EABI v7a System Image 23r4
* Extras/
  * [missing] Android Support Repository 22
  * Android Support Library 23.0.1 (update available r23.1.1??)

r81:

    $ JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64 tools/bin/sdkmanager --list
    Installed packages:
      Path                                         | Version | Description                         | Location                                
      -------                                      | ------- | -------                             | -------                                 
      build-tools;23.0.2                           | 23.0.2  | Android SDK Build-Tools 23.0.2      | build-tools/android-6.0.1/              
      docs                                         | 1       | Documentation for Android SDK       | docs/                                   
      extras;android;support                       | 23.0.1  | Android Support Library, rev 23.0.1 | extras/android/support/                 
      platform-tools                               | 23.0.0  | Android SDK Platform-Tools 23       | platform-tools/                         
      platforms;android-23                         | 1       | Android SDK Platform 23             | platforms/android-6.0.1/                
      system-images;android-23;default;armeabi-v7a | 4       | ARM EABI v7a System Image           | system-images/android-6.0.1/armeabi-v7a/
    
    Available Updates:
      ID                   | Installed | Available
      -------              | -------   | -------  
      platforms;android-23 | 1         | 3  


### ccache usage

    N/A


### build stats

Using i7-4500U (2x1.80GHz + HT), SSD disk:

* make sdk: 03h25mn
* make win_sdk: 0h50mn

Using i5-6300HQ (4x2.30GHz), SSD disk:

* make sdk: make completed successfully (01:28:39 (hh:mm:ss))
* make win_sdk: make completed successfully (23:42 (mm:ss))
* du -sh wd/: 138G
* du -sh wd/.repo/: 73G
* repos download time: ~6h

## Automated build recipe

[[https://gitlab.com/android-rebuilds/auto/tree/master/sdk-6.0.1]]
