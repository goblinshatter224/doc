**SDK 6.0.0r1 (GNU/Linux, Windows)**

[[!toc]]

## Recreate matching build environment

The current reference build environment for this version is [[Ubuntu 14.04|Build_environments]].

Source code was pushed between 2015-10-05 ([manifest commit](https://android.googlesource.com/platform/manifest/+/refs/heads/android-6.0.0_r1)) and 2015-10-08 (starting this page).

## Build dependencies

    # http://source.android.com/source/initializing.html + fixes
    dpkg --add-architecture i386
    apt-get update
    apt-get -y install openjdk-7-jdk
    apt-get -y install bison git gperf libxml2-utils make python-networkx zip unzip
    apt-get -y install g++-multilib lib32ncurses5 zlib1g-dev:i386

    # Windows dependencies
    # https://sites.google.com/a/android.com/tools/build  2015-07-23
    apt-get -y install mingw32 tofrodos


## Prepare source tree

Install `repo`:

    sudo apt-get install curl ca-certificates git python
    mkdir ~/bin/
    curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
    chmod a+x ~/bin/repo

    # avoid prompts
    git config --global user.email "you@example.com"
    git config --global user.name "Your Name"
    git config --global color.ui true

Checkout the source:

    mkdir ~/wd/
    cd ~/wd/
    ~/bin/repo init -u https://android.googlesource.com/platform/manifest -b android-6.0.0_r1
    time ~/bin/repo sync
    # 35mn with very good connection
    # 32GB (inc. 19GB .repo, from scratch)

## Build the SDK

    # Optional: reserve 10GB for ccache if you're investigating how to build or patching something
    # Cf. https://source.android.com/source/initializing.html#ccache
    export USE_CCACHE=1
    prebuilts/misc/linux-x86/ccache/ccache -M 10G

    . build/envsetup.sh
    lunch sdk-eng
    time make sdk -j$(nproc) showcommands
    # 1h42, 74GB (inc. 19GB .repo), 7.5GB cache size, 8GB RAM direly needed during the 'docdroid' phase
    # => out/host/linux-x86/sdk/sdk/android-sdk_eng.android_linux-x86.zip
    # rebuilt in 55mn using 7.5GB ccache

    time make win_sdk -j$(nproc)
    # 30mn, 86GB (inc. 19GB .repo), ccache untouched
    # => out/host/windows/sdk/sdk/android-sdk_eng.android_windows.zip


## TODO

Rebuild the `prebuilts/` sub-directories.


## Error messages and resolutions

    Host TableGen:  (gen-opt-parser-defs) <= frameworks/compile/mclinker/tools/mcld/Options.td
    Building Renderscript compiler (llvm-rs-cc) Option tables with tblgen
    out/host/linux-x86/bin/llvm-tblgen: error while loading shared libraries: libncurses.so.5: cannot open shared object file: No such file or directory
    Install: out/host/linux-x86/bin/clang-tblgen
    make: *** [out/host/linux-x86/gen/EXECUTABLES/ld.mc_intermediates/Options.inc] Error 127
    make: *** Waiting for unfinished jobs....
    Host TableGen:  (gen-opt-parser-defs) <= frameworks/compile/slang/RSCCOptions.td
    out/host/linux-x86/bin/llvm-tblgen: error while loading shared libraries: libncurses.so.5: cannot open shared object file: No such file or directory
    make: *** [out/host/linux-x86/gen/EXECUTABLES/llvm-rs-cc_intermediates/RSCCOptions.inc] Error 127

=> `apt-get install lib32ncurses5`

-----

    Exception in thread "main" java.lang.NoClassDefFoundError: org/bouncycastle/util/Store
            at java.lang.Class.getDeclaredMethods0(Native Method)
            at java.lang.Class.privateGetDeclaredMethods(Class.java:2615)
            at java.lang.Class.getMethod0(Class.java:2856)
            at java.lang.Class.getMethod(Class.java:1668)
            at sun.launcher.LauncherHelper.getMainMethod(LauncherHelper.java:494)
            at sun.launcher.LauncherHelper.checkAndLoadMain(LauncherHelper.java:486)
    Caused by: java.lang.ClassNotFoundException: org.bouncycastle.util.Store
            at java.net.URLClassLoader$1.run(URLClassLoader.java:366)
            at java.net.URLClassLoader$1.run(URLClassLoader.java:355)
            at java.security.AccessController.doPrivileged(Native Method)
            at java.net.URLClassLoader.findClass(URLClassLoader.java:354)
            at java.lang.ClassLoader.loadClass(ClassLoader.java:425)
            at sun.misc.Launcher$AppClassLoader.loadClass(Launcher.java:308)
            at java.lang.ClassLoader.loadClass(ClassLoader.java:358)
            ... 6 more
    make: *** [out/target/product/generic/obj/APPS/DevelopmentSettings_intermediates/package.apk] Error 1
    make: *** Waiting for unfinished jobs....

or

    JarJar: out/target/common/obj/JAVA_LIBRARIES/core-libart_intermediates/classes-jarjar.jar
    Exception in thread "main" java.lang.reflect.InvocationTargetException
            at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
            at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:57)
            at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
            at java.lang.reflect.Method.invoke(Method.java:606)
            at com.tonicsystems.jarjar.MainUtil.runMain(MainUtil.java:37)
            at com.tonicsystems.jarjar.Main.main(Main.java:50)
    Caused by: java.lang.NoClassDefFoundError: org/objectweb/asm/commons/Remapper
            at com.tonicsystems.jarjar.Main.process(Main.java:93)
            ... 6 more
    Caused by: java.lang.ClassNotFoundException: org.objectweb.asm.commons.Remapper
            at java.net.URLClassLoader$1.run(URLClassLoader.java:366)
            at java.net.URLClassLoader$1.run(URLClassLoader.java:355)
            at java.security.AccessController.doPrivileged(Native Method)
            at java.net.URLClassLoader.findClass(URLClassLoader.java:354)
            at java.lang.ClassLoader.loadClass(ClassLoader.java:425)
            at sun.misc.Launcher$AppClassLoader.loadClass(Launcher.java:308)
            at java.lang.ClassLoader.loadClass(ClassLoader.java:358)
            ... 7 more
    make: *** [out/target/common/obj/JAVA_LIBRARIES/core-libart_intermediates/classes-jarjar.jar] Error 1

or

    Exception in thread "main" java.lang.NoClassDefFoundError: org/objectweb/asm/ClassVisitor
            at java.lang.Class.getDeclaredMethods0(Native Method)
            at java.lang.Class.privateGetDeclaredMethods(Class.java:2615)
            at java.lang.Class.getMethod0(Class.java:2856)
            at java.lang.Class.getMethod(Class.java:1668)
            at sun.launcher.LauncherHelper.getMainMethod(LauncherHelper.java:494)
            at sun.launcher.LauncherHelper.checkAndLoadMain(LauncherHelper.java:486)
    Caused by: java.lang.ClassNotFoundException: org.objectweb.asm.ClassVisitor
            at java.net.URLClassLoader$1.run(URLClassLoader.java:366)
            at java.net.URLClassLoader$1.run(URLClassLoader.java:355)
            at java.security.AccessController.doPrivileged(Native Method)
            at java.net.URLClassLoader.findClass(URLClassLoader.java:354)
            at java.lang.ClassLoader.loadClass(ClassLoader.java:425)
            at sun.misc.Launcher$AppClassLoader.loadClass(Launcher.java:308)
            at java.lang.ClassLoader.loadClass(ClassLoader.java:358)
            ... 6 more
    make: *** [out/target/common/obj/JAVA_LIBRARIES/framework_intermediates/classes-full-debug.jar] Error 1


=> `apt-get install unzip` + restart the build from scratch

Missing `unzip` causes non-fatal errors where the build fails to unzip existing `.jar` dependencies for re-bundling in new `jar`s.

-----

    JACK_VM_COMMAND="java  -Dfile.encoding=UTF-8 -Xms2560m -XX:+TieredCompilation -jar out/host/linux-x86/framework/jack-launcher.jar " JACK_JAR="out/host/linux-x86/framework/jack.jar" out/host/linux-x86/bin/jack-admin start-server
    out/host/linux-x86/bin/jack-admin: line 27: USER: unbound variable

=> if using Docker, define the `USER` environment variable (not set by default), e.g. `export USER=$(whoami)`


## Notes

### Build result recap

* Tools/
  * [missing] Android [[SDK Tools]]
  * Android SDK Platform-tools 23 (update available r23.0.1??)
  * Android SDK Build-tools 23.0.1
* Android 6.0 (API 23)/
  * Documentation for Android SDK 23r1
  * SDK Platform 23r1
  * Samples for SDK 23r3
  * ARM EABI v7a System Image 23r3
* Extras/
  * [missing] Android Support Repository 22
  * Android Support Library 23 (update available r23.0.1??)


### ccache usage

    $ wd/prebuilts/misc/linux-x86/ccache/ccache -s
    cache directory                     /home/android/.ccache
    cache hit (direct)                  1079
    cache hit (preprocessed)               0
    cache miss                         14686
    called for link                      806
    unsupported source language          418
    files in cache                     44982
    cache size                           7.5 Gbytes
    max cache size                      10.0 Gbytes

    $ prebuilts/misc/linux-x86/ccache/ccache -s
    cache directory                     /home/android/.ccache
    cache hit (direct)                 15648
    cache hit (preprocessed)              27
    cache miss                         14688
    called for link                     1286
    unsupported source language          836
    files in cache                     45036
    cache size                           7.5 Gbytes
    max cache size                      10.0 Gbytes
